<?php
/**
<form action="" method="post">
<h3>Social Medias:</h3>
<input type="radio" name="rbSocialMedias" value="instagram" checked> Instagram<br>
<input type="radio" name="rbSocialMedias" value="twitter"> Twitter<br>
<h3>Types:</h3>
<input type="radio" name="rbTypes" value="t1" checked> Likes<br>
<input type="radio" name="rbTypes" value="t2"> Retweet/Comments<br>
<input type="radio" name="rbTypes" value="t3"> Both<br><br>
<input type="submit">
</form>
*/
$conn = mysqli_connect("localhost","root","","u522840214_tw_wl");
$myfile = fopen("pajek.net", "w") or die("Unable to open file!");
$postMap = [];
$userMap = [];

if(isset($_POST['rbSocialMedias'])){
	if($_POST['rbSocialMedias']!=""){
		if($_POST['rbSocialMedias']=="instagram")
		getVertices("ig_accounts","ig_posts","username","link");
		else 
		getVertices("tw_accounts","tw_tweets","screen_name","id");
	}
	if($_POST['rbTypes']!=""){
		if($_POST['rbTypes']=="t1") //Likes
		getEdges($_POST['rbSocialMedias'],"t1");
		else if($_POST['rbTypes']=="t2") //Retweet/Comments
		getEdges($_POST['rbSocialMedias'],"t2");
		else //Both
		getEdges($_POST['rbSocialMedias'],"t3");
	}
}

function getVertices($account, $type, $field1, $field2){
	global $conn, $userMap, $postMap;
	$userMap = [];
	$postMap = [];
	$ctr = 1;
	$temp = mysqli_query($conn,"select * from $account");
	$buff = "";
	
	//get all user
	while($row = mysqli_fetch_array($temp)){
		$userMap[$row['id']] = $ctr;
		$buff .= "$ctr \"$row[$field1]\"\n";
		$ctr++;
	}
	
	//if two mode, get all post
	if ($type !== null) {
		$ctrUser = $ctr - 1;
		$temp = mysqli_query($conn,"select * from $type");
		$post = [];
		while($row = mysqli_fetch_array($temp)){
			$postMap[$row['id']] = $ctr;
			$buff .= "$ctr \"$row[$field2]\"\n";
			$ctr++;
		}
		fwrite($GLOBALS['myfile'], "*Vertices $ctr $ctrUser\n".$buff);
	} else {
		fwrite($GLOBALS['myfile'], "*Vertices $ctr\n".$buff);
	}
	
}
function getEdges($socmed, $type){
	global $conn, $userMap, $postMap;
	fwrite($GLOBALS['myfile'],"*Arcs\n");
	if($socmed=="instagram"){
		$temp="";
		if($type=="t1")
		$temp=mysqli_query($conn,"select * from ig_likes order by id_account");
		else if($type=="t2")
		$temp=mysqli_query($conn,"select * from ig_comments order by id_account");
		else 
		$temp=mysqli_query($conn,"select * from ig_comments,ig_likes where ig_comments.id_account=ig_likes.id_account order by ig_comments.id_account");
		
		while($row=mysqli_fetch_array($temp)){
			//echo $row["id_account"]." ".(intval($row["id_post"])+$GLOBALS['ctrUser'])." 1<br>";
			$txt=$row["id_account"]." ".(intval($row["id_post"])+$GLOBALS['ctrUser'])." 1\n";
			fwrite($GLOBALS['myfile'],$txt);
		}
		
		//ambil post owner
		$temp=mysqli_query($conn,"select * from ig_posts order by id");
		while($row=mysqli_fetch_array($temp)){
			//echo $row["id_account"]." ".(intval($row["id_post"])+$GLOBALS['ctrUser'])." 1<br>";
			$txt=(intval($row["id"])+$GLOBALS['ctrUser'])." ".$row["id_ig_owner"]." 1\n";
			fwrite($GLOBALS['myfile'],$txt);
		}
	}
	else {
		$SQLs = [
			'likes' => "SELECT id_tweet, id_account, 1 as jumlah FROM tw_likes",
			'retweets' => "SELECT id_tweet, id_account, 1 as jumlah FROM tw_retweets",
			'replies' => <<<SQL
				SELECT reply_tweets.id as id_tweet, orig_tweet.tweet_owner as id_account, 1 as jumlah
				FROM tw_tweets reply_tweets, tw_tweets orig_tweet
				WHERE reply_tweets.parent_tweet = orig_tweet.id
			SQL
		];
		$unions = [];
		foreach (explode(",", $type) as $type) {
			$unions[] = $SQLs[$type];
		}
		$unions = implode(" UNION ALL ", $unions);
		$finalSQL = "SELECT u.id_tweet, u.id_account, COUNT(*) as jumlah FROM ($unions) u GROUP BY 1, 2";
		$temp = mysqli_query($conn, $finalSQL);
		while ($row = mysqli_fetch_array($temp)) {
			$userID = $userMap[$row['id_account']];
			$postID = $postMap[$row['id_tweet']];
			$txt = $userID.' '.$postID.' '.$row['jumlah']."\n";
			fwrite($GLOBALS['myfile'],$txt);
		}
		$temp = mysqli_query($conn,"select * from tw_tweets");
		while($row = mysqli_fetch_array($temp)) {
			$userID = $userMap[$row['tweet_owner']];
			$postID = $postMap[$row['id']];
			$txt = $userID.' '.$postID." 1\n";
			fwrite($GLOBALS['myfile'],$txt);
		}
	}
}
function getOneModeEdges($socmed, $type) {
	global $conn, $userMap, $postMap;
	fwrite($GLOBALS['myfile'], "*Arcs\n");
	if ($socmed === 'instagram') {

		return;
	}
	$SQLs = [
		'likes' => <<<SQL
			SELECT tw_accounts.id as dari, id_account as ke, 1 as jumlah
			FROM tw_likes, tw_tweets, tw_accounts
			WHERE tw_accounts.id = tw_tweets.tweet_owner
				AND tw_tweets.id = tw_likes.id_tweet
		SQL,
		'retweets' => <<<SQL
			SELECT tw_accounts.id as dari, id_account as ke, 1 as jumlah
			FROM tw_retweets, tw_tweets, tw_accounts
			WHERE tw_accounts.id = tw_tweets.tweet_owner
				AND tw_tweets.id = tw_retweets.id_tweet
		SQL,
		'replies' => <<<SQL
			SELECT reply_tweets.tweet_owner as dari, orig_tweet.tweet_owner as ke, 1 as jumlah
			FROM tw_tweets reply_tweets, tw_tweets orig_tweet
			WHERE reply_tweets.parent_tweet = orig_tweet.id
		SQL
	];
	$unions = [];
	foreach (explode(",", $type) as $type) {
		$unions[] = $SQLs[$type];
	}
	$unions = implode(" UNION ALL ", $unions);
	$finalSQL = "SELECT u.dari, u.ke, COUNT(*) as jumlah FROM ($unions) u GROUP BY 1, 2";
	$temp = mysqli_query($conn, $finalSQL);
	echo mysqli_error($conn);
	while ($row = mysqli_fetch_array($temp)) {
		$dariID = $userMap[$row['dari']];
		$keID = $userMap[$row['ke']];
		$txt = $dariID.' '.$keID.' '.$row['jumlah']."\n";
		fwrite($GLOBALS['myfile'],$txt);
	}
}
getVertices("tw_accounts","tw_tweets","name","id");
getOneModeEdges("twitter","likes,retweets,replies");
fclose($GLOBALS['myfile']);