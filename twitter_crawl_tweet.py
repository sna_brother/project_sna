import mysql.connector
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.webdriver import FirefoxProfile
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os, sys, time, tempfile
from datetime import datetime
from dateutil.parser import parse as parseDate

from commons import *
import config

tempfile.tempdir = config.TEMPDIR

cursor = execute_sql(
    "SELECT tw_tweets.id, name " +
    "FROM tw_tweets " +
    "JOIN tw_accounts ON tw_tweets.tweet_owner = tw_accounts.id " +
    "WHERE tw_tweets.status = 0 LIMIT 10"
)
selectedTweets = cursor.fetchall()
cursor.execute("UPDATE tw_tweets SET status=1 WHERE id IN (%s)" % ",".join([str(tweet[0]) for tweet in selectedTweets]))
#db.commit()
if (selectedTweets is None):
    raise Exception("Ehm kehabisan tweets!")

profile = FirefoxProfile(config.FIREFOX_PROFILE_LOCATION)
profile.set_preference("permissions.default.image", 2)
profile.set_preference("dom.serviceWorker.enabled", False)
driver = webdriver.Firefox(firefox_profile=profile)

def parse_user_info(info: str):
    userInfo = info.split("\n")
    if (len(userInfo) == 1):
        userInfo.append(userInfo[0])
        userInfo[0] = ''
    elif (len(userInfo) > 2):
        userName = userInfo.pop()
        screenName = " ".join(userInfo)
        userInfo = [screenName, userName]
    userInfo[0] = userInfo[0].encode("ascii", "ignore").decode() #biar unicode (huruf aneh aneh) gak masuk DB, gak begitu penting
    userInfo[1] = userInfo[1][1:]
    return userInfo

insertAccSQL = "INSERT INTO tw_accounts(screen_name, name) VALUES (%s, %s)"
insertTweetSQL = "INSERT IGNORE INTO tw_tweets(id, tweet_owner) VALUES(%s, %s)"
updateParentTweetSQL = "UPDATE tw_tweets SET parent_tweet = %s WHERE id = %s"
checkAccExistsSQL = "SELECT name, id FROM tw_accounts WHERE name IN (%s)"
insertLikeSQL = "INSERT INTO tw_likes VALUES(%s, %s)"
insertRetweetSQL = "INSERT INTO tw_retweets VALUES(%s, %s)"
for tweet in selectedTweets:
    print("Opening %s..." % (tweet[0],))
    #dapatkan tweet lain
    driver.get("https://twitter.com/" + tweet[1] + "/status/" + str(tweet[0]))
    #driver.get("https://twitter.com/cheeseburgers52/status/1190370637753307137")
    #driver.get("https://twitter.com/kaskus/status/1189809738546065408")
    time.sleep(10)
    otherUsersInfoList = {} # key adalah username, untuk saat ini berisi screen name, yang nanti diisi ID dari DB
    otherTweetInfoList = {} # key adalah tweet ID, untuk saat ini berisi owner username saja
    gotPrimaryTweet = False
    lastScroll = 0
    currentScroll = 1
    while (currentScroll != lastScroll):
        artikelList = driver.find_elements_by_tag_name("article")
        for i in range(len(artikelList)):
            artikel = artikelList[i]
            classes = artikel.get_attribute("class")
            if (len(classes) < 24): #tweet utama punya kelas lebih sedikit
                if (i == 0 or gotPrimaryTweet):
                    continue
                gotPrimaryTweet = True
                waktuEl = artikelList[i - 1].find_element_by_tag_name("time")
                tweetLink: str = waktuEl.find_element_by_xpath("..").get_attribute("href")
                tweetID = tweetLink[tweetLink.rfind("/") + 1:]
                execute_sql(updateParentTweetSQL, (tweetID, tweet[0]))
                print("Updating parent for %s to %s" % (tweet[0], tweetID))
            else:
                userInfo = artikel.find_element_by_css_selector("div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a > div")
                userInfo = parse_user_info(userInfo.text)
                otherUsersInfoList[userInfo[1]] = [userInfo[0]]
                tweetLink: str = artikel.find_element_by_tag_name("time").find_element_by_xpath("..").get_attribute("href")
                tweetID = tweetLink[tweetLink.rfind("/") + 1:]
                otherTweetInfoList[tweetID] = [userInfo[1]]
        lastScroll = currentScroll
        driver.execute_script("window.scrollBy(0, 700);")
        time.sleep(3)
        currentScroll = driver.execute_script("return document.body.scrollTop")
    if (len(otherUsersInfoList) > 0):
        cursor = execute_sql(checkAccExistsSQL % ('"' + '","'.join(otherUsersInfoList.keys()) + '"',))
        for userInfo in cursor.fetchall():
            otherUsersInfoList[userInfo[0]].append(userInfo[1])
        print("Inserted new account :", end='')
        for userName in otherUsersInfoList:
            if len(otherUsersInfoList[userName]) > 1:
                continue
            screenName = otherUsersInfoList[userName][0]
            cursor = execute_sql(insertAccSQL, (screenName, userName))
            otherUsersInfoList[userName].append(cursor.lastrowid)
            print(" %s (%s)" % (userName, cursor.lastrowid), end='')
        print()
        #insertParam = [(otherUsersInfoList[user][0], user) for user in otherUsersInfoList if len(otherUsersInfoList[user]) == 1]
    if (len(otherTweetInfoList) > 0):
        for tweetID in otherTweetInfoList:
            ownerUserName = otherTweetInfoList[tweetID][0]
            ownerUserID = otherUsersInfoList[ownerUserName][1]
            execute_sql(insertTweetSQL, (tweetID, ownerUserID))
        print("Inserted %s other tweets from %s" % (len(otherTweetInfoList), tweet[0]))
    #db.commit()
    
    #dapet daftar like
    driver.get("https://twitter.com/" + tweet[1] + "/status/" + str(tweet[0]) + "/likes")
    #driver.get("https://twitter.com/kaskus/status/1189809738546065408/likes")
    #driver.get("https://twitter.com/Smancosoft/status/278366800746524672/likes")
    time.sleep(10)
    driver.execute_script("window.scrollParent = document.querySelector(\"div[aria-label='Timeline: Disukai oleh']\")")
    if (driver.execute_script("return window.scrollParent") is not None):
        driver.execute_script("window.scrollParent = window.scrollParent.parentNode.parentNode.parentNode")
        lastScroll = 0
        currentScroll = 1
        otherUsersInfoList = {}
        while (currentScroll != lastScroll):
            likesEl = driver.find_elements_by_css_selector("div[aria-label='Timeline: Disukai oleh'] > div > div > *")
            for likeEl in likesEl:
                try:
                    userInfo = likeEl.find_element_by_css_selector("div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1)")
                except NoSuchElementException:
                    continue
                userInfo = parse_user_info(userInfo.text)
                otherUsersInfoList[userInfo[1]] = [userInfo[0]]
                    
            lastScroll = currentScroll
            driver.execute_script("window.scrollParent.scrollBy(0, 700);")
            time.sleep(3)
            currentScroll = driver.execute_script("return window.scrollParent.scrollTop")

        if (len(otherUsersInfoList) > 0):
            cursor = execute_sql(checkAccExistsSQL % ('"' + '","'.join(otherUsersInfoList.keys()) + '"',))
            for userInfo in cursor.fetchall():            
                otherUsersInfoList[userInfo[0]].append(userInfo[1])
            print("Inserted new account :", end='')
            for userName in otherUsersInfoList:
                if len(otherUsersInfoList[userName]) > 1:
                    execute_sql(insertLikeSQL, (tweet[0], otherUsersInfoList[userName][1]))
                    continue
                screenName = otherUsersInfoList[userName][0]
                cursor = execute_sql(insertAccSQL, (screenName, userName))
                print(" %s (%s)" % (userName, cursor.lastrowid), end='')
                cursor.execute(insertLikeSQL, (tweet[0], cursor.lastrowid))
            print()
            print("Inserted %s likes for %s" % (len(otherUsersInfoList), tweet[0]))
    #db.commit()

    #dapet daftar retweet
    driver.get("https://twitter.com/" + tweet[1] + "/status/" + str(tweet[0]) + "/retweets")
    #driver.get("https://twitter.com/kaskus/status/1189809738546065408/retweets")
    #driver.get("https://twitter.com/Smancosoft/status/278366800746524672/likes")
    time.sleep(10)
    driver.execute_script("window.scrollParent = document.querySelector(\"div[aria-label='Timeline: Di-Retweet oleh']\")")
    if (driver.execute_script("return window.scrollParent") is not None):
        driver.execute_script("window.scrollParent = window.scrollParent.parentNode.parentNode.parentNode")
        lastScroll = 0
        currentScroll = 1
        otherUsersInfoList = {}
        while (currentScroll != lastScroll):
            likesEl = driver.find_elements_by_css_selector("div[aria-label='Timeline: Di-Retweet oleh'] > div > div > *")
            for likeEl in likesEl:
                try:
                    userInfo = likeEl.find_element_by_css_selector("div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1)")
                except NoSuchElementException:
                    continue
                userInfo = parse_user_info(userInfo.text)
                otherUsersInfoList[userInfo[1]] = [userInfo[0]]
            lastScroll = currentScroll
            driver.execute_script("window.scrollParent.scrollBy(0, 700);")
            time.sleep(3)
            currentScroll = driver.execute_script("return window.scrollParent.scrollTop")

        if (len(otherUsersInfoList) > 0):
            cursor = execute_sql(checkAccExistsSQL % ('"' + '","'.join(otherUsersInfoList.keys()) + '"',))
            for userInfo in cursor.fetchall():
                otherUsersInfoList[userInfo[0]].append(userInfo[1])
            print("Inserted new account :", end='')
            for userName in otherUsersInfoList:
                if len(otherUsersInfoList[userName]) > 1:
                    execute_sql(insertRetweetSQL, (tweet[0], otherUsersInfoList[userName][1]))
                    continue
                screenName = otherUsersInfoList[userName][0]
                cursor = execute_sql(insertAccSQL, (screenName, userName))
                print(" %s (%s)" % (userName, cursor.lastrowid), end='')
                cursor.execute(insertRetweetSQL, (tweet[0], cursor.lastrowid))
            print()
            print("Inserted %s retweets for %s" % (len(otherUsersInfoList), tweet[0]))
    
    execute_sql("UPDATE tw_tweets SET status=2 WHERE id = %s", (tweet[0],))
    #db.commit()

driver.close()
driver.quit()
print("Done.")
