import mysql.connector
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.webdriver import FirefoxProfile
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from dateutil.parser import parse as parseDate
import sys, tempfile
from bs4 import BeautifulSoup as bs
import pandas as pd
import unittest, time, re
import config
from commons import *

tempfile.tempdir = config.TEMPDIR

cursor = execute_sql("SELECT username, id FROM ig_accounts WHERE status = 0 LIMIT 3")
selectedUsers = cursor.fetchall()
if (len(selectedUsers) == 0):
    raise Exception("Ehm kehabisan akun Instagram!")
print("UPDATE ig_accounts SET status=1 WHERE username IN (%s)" % ",".join(['"' + user[0] + '"' for user in selectedUsers]))
cursor.execute("UPDATE ig_accounts SET status=1 WHERE username IN (%s)" % ",".join(['"' + user[0] + '"' for user in selectedUsers]))


profile = FirefoxProfile(config.FIREFOX_PROFILE_LOCATION)
profile.set_preference("permissions.default.image", 2)
browser = webdriver.Firefox(firefox_profile=profile)
insertSQL = "INSERT IGNORE INTO ig_posts(link, id_ig_owner) VALUES(%s, %s)"
posts = dict()
orangke = 1
for selectedUser in selectedUsers:
    browser.get("https://www.instagram.com/" + selectedUser[0] + "/feed")
    time.sleep(5)
    articles = browser.find_elements_by_tag_name("article")
    if(len(articles) > 0):
        try:
            no_post = articles[0].find_elements_by_tag_name('h1')
            if(len(no_post) > 0):
                print(selectedUser[0] + " : " + no_post[0].text)
                if(no_post[0].text != "No Posts Yet"):
                    raise NoSuchElementException

            private = articles[0].find_elements_by_tag_name("h2")
            if(len(private) > 0):
                print(selectedUser[0] + " : " + private[0].text)
                if(private[0].text != "This Account is Private"):
                    raise NoSuchElementException
        except NoSuchElementException:
            satuBulanYangLalu = int(time.time() - 2592000)
            scroll = True
            lastScroll = 0
            currentScroll = 1
            browser.execute_script("window.scrollParent = document.documentElement")

            while(scroll):
                #ctr=0
                for artikel in browser.find_elements_by_tag_name("article"):
                    waktuEl = artikel.find_element_by_tag_name("time")
                    waktu = parseDate(waktuEl.get_attribute("datetime")).timestamp()
                    if(waktu > satuBulanYangLalu):
                        a_tag = waktuEl.find_element_by_xpath("..")
                        link = a_tag.get_attribute("href")[28:-1]
                        #ctr+=1
                        print(link)
                        posts[link] = link
                    else:
                        scroll = False
                print("orang ke- " + str(orangke) + " === tot post : " + str(len(posts)))
                lastScroll = currentScroll
                browser.execute_script("window.scrollParent.scrollBy(0,1000);")
                time.sleep(3)
                currentScroll = browser.execute_script("return window.scrollParent.scrollTop")
                time.sleep(1)
                if(currentScroll == lastScroll):
                    scroll = False
            ctr = 1
            for link in posts:
                execute_sql(insertSQL, (link, selectedUser[1]))
                print(str(ctr) + " /" + str(len(posts))+ " : " + link + " has been inserted")
                ctr+=1

    temp_id = []
    temp_id.append(selectedUser[0])
    execute_sql("UPDATE ig_accounts SET status=2 WHERE username in (%s)" % ",".join(['"' + str(un) + '"' for un in temp_id]))
    print(str(orangke)+" /" + str(len(selectedUsers)) + " orang, username : " + selectedUser[0])
    orangke+=1
    
browser.close()
browser.quit()