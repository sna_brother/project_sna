import mysql.connector
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.webdriver import FirefoxProfile
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from dateutil.parser import parse as parseDate
import sys, tempfile
from bs4 import BeautifulSoup as bs
import pandas as pd
import unittest, time, re
import config
from commons import *

class Person:
    def __init__(self, username, name, link):
        self.username = username
        self.name = name
        self.link = link

def getUser(user):
    cursor = execute_sql(
        "SELECT * " +
        "FROM ig_accounts " +
        "WHERE username = '"+ user +"' LIMIT 1"
    )
    return cursor.fetchall()[0]

tempfile.tempdir = config.TEMPDIR
cursor = execute_sql(
    "SELECT ig_posts.link, ig_posts.id " +
    "FROM ig_posts " +
    "JOIN ig_accounts ON ig_posts.id_ig_owner = ig_accounts.id " +
    "WHERE ig_posts.status = 0 LIMIT 10"
)
selectedPosts = cursor.fetchall()
cursor.execute("UPDATE ig_posts SET status=1 WHERE link IN (%s)" % ",".join(['"'+post[0]+'"' for post in selectedPosts]))
if (selectedPosts is None):
    raise Exception("Ehm kehabisan posts!")

profile = FirefoxProfile(config.FIREFOX_PROFILE_LOCATION)
profile.set_preference("permissions.default.image", 2)
browser = webdriver.Firefox(firefox_profile=profile)

insertAccSQL = "INSERT IGNORE INTO ig_accounts(username, name) VALUES (%s, %s)"
insertLikeSQL = "INSERT IGNORE INTO ig_likes VALUES(%s, %s)"
insertCommentSQL = "INSERT IGNORE INTO ig_comments VALUES(%s, %s)"
postke = 1

for post in selectedPosts:
    browser.get("https://www.instagram.com/p/" + post[0]+"/")
    time.sleep(5)
    articles = browser.find_elements_by_tag_name("article")
    if(len(articles) > 0):
        try:
            video = browser.find_elements_by_tag_name('video')
            if(len(video) == 0):
                raise NoSuchElementException
        except NoSuchElementException:
            # LIKE
            browser.execute_script("document.getElementsByClassName('zV_Nj')[0].click()")
            time.sleep(10)
            
            lastScroll = 0
            currentScroll = 1
            browser.execute_script("var a = document.getElementsByClassName('i0EQd');window.scrollParent = a[0].children[0];")
            orangke = 1
            person = dict()
            while (currentScroll != lastScroll):
                for div in browser.find_elements_by_css_selector(".i0EQd > div:nth-child(1) > div:nth-child(1) > *"):
                    temp = div.find_element_by_css_selector("div:nth-child(2)")
                    user = temp.text.split("\n")
                    if(len(user) == 1):
                        user.append(user[0])

                    person[user[0]] = Person(user[0], user[1], post[0])
                    print(user[0] + ", " + user[1])
                print("======== " + str(len(person)) + " ========= postke : "+str(postke) +":  "+ post[0] +"=======================")
                lastScroll = currentScroll
                browser.execute_script("window.scrollParent.scrollBy(0,700);")
                time.sleep(3)
                currentScroll = browser.execute_script("return window.scrollParent.scrollTop")
                time.sleep(1)
                    
            for orang in person:
                execute_sql(insertAccSQL, (person[orang].username, person[orang].name))
                user_now = getUser(person[orang].username)
                execute_sql(insertLikeSQL, (post[1], user_now[0]))
                print(str(orangke) + "/ " + str(len(person)) + " orang,post : " + str(postke) + ", nama : " + person[orang].username + ", " + person[orang].name + " has been inserted")
                orangke+=1
            # COMMENT
            time.sleep(3)
            print("===================== Masuk comment =======================")
            browser.get("https://www.instagram.com/p/" + post[0]+"/comments")
            time.sleep(5)
            for div in browser.find_elements_by_tag_name('h3'):
                username = div.text
                execute_sql(insertAccSQL, (username, username))
                user_now = getUser(username)
                execute_sql(insertCommentSQL, (post[1], user_now[0]))
                print(username + ", " + username + " has been inserted")
            time.sleep(1)
            
    postke+=1
    temp_post = []
    temp_post.append(post[0])
    time.sleep(1)
    execute_sql("UPDATE ig_posts SET status=2 WHERE link in (%s)" % ",".join(['"' + str(p) + '"' for p in temp_post]))
    print("========================== POST KE  " + post[0] + " ============================")
    time.sleep(5)
    
browser.close()
browser.quit()