#crawling profil untuk mendapatkan daftar tweet yang ada dalam 1 bulan terakhir
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.webdriver import FirefoxProfile
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os, sys, time, tempfile
from datetime import datetime
from dateutil.parser import parse as parseDate

from commons import *
import config

satuBulanYangLalu = int(time.time() - 2592000)
tempfile.tempdir = config.TEMPDIR

cursor = execute_sql("SELECT id, name FROM tw_accounts WHERE status = 0 LIMIT 1")
selectedUsers = cursor.fetchall()
if (selectedUsers is None):
    raise Exception("Ehm kehabisan akun twitter!")
cursor.execute("UPDATE tw_accounts SET status=1 WHERE id IN (%s)" % ",".join([str(user[0]) for user in selectedUsers]))

profile = FirefoxProfile(config.FIREFOX_PROFILE_LOCATION)
profile.set_preference("permissions.default.image", 2)
profile.set_preference("dom.serviceWorker.enabled", False)
driver = webdriver.Firefox(firefox_profile=profile)

#driver.base_url = "https://twitter.com/dokterpedia_ind/following"
#browser.base_url = "https://twitter.com/dokterpedia_ind/followers"
#driver.get(driver.base_url)
#if (driver.current_url[:25] == 'https://twitter.com/login'):
#    print("Logging in...")
#    driver.find_element_by_css_selector('.signin-wrapper input[name="session[username_or_email]"]').send_keys(config.TWITTER_USERNAME)
#    time.sleep(1)
#    driver.find_element_by_css_selector('.signin-wrapper input[name="session[password]"]').send_keys(config.TWITTER_PASSWORD)
#    driver.find_element_by_css_selector('.signin-wrapper button').submit()
#time.sleep(10)

otherUsers = []
for user in selectedUsers:
    otherUsers.append(user[1])

insertSQL = "INSERT IGNORE INTO tw_tweets(id, tweet_owner) VALUES(%s, %s)"
insertAccSQL = "INSERT IGNORE INTO tw_accounts(screen_name, name) VALUES (%s, %s)"
for selectedUser in selectedUsers:
    print("Crawling %s..." % (selectedUser[1],))
    driver.get("https://twitter.com/" + selectedUser[1] + "/with_replies")
    time.sleep(10)
    lastTweetTime = time.time()
    while lastTweetTime > satuBulanYangLalu:
        for artikel in driver.find_elements_by_tag_name("article"):
            try:
                waktuEl = artikel.find_element_by_tag_name("time")
            except NoSuchElementException: #biasa kejadian jika ini adalah iklan (tweet promosi)
                continue
            waktu = parseDate(waktuEl.get_attribute("datetime"))
            if (waktu.timestamp() >= lastTweetTime): #ini berarti sudah di ambil
                continue
            infoTambahan = artikel.find_element_by_css_selector("div:nth-child(1) > div:nth-child(1)").text
            if (len(infoTambahan) > 0): # tweet sematan, retweet
                if (infoTambahan == "Tweet Sematan"): #jika tweet sematan, skip
                    continue
                #jika retweet, atau like, tambah akunnya, bukan tweetnya
                userInfo = artikel.find_element_by_xpath("//div/div[2]/div[2]/div[1]/div[1]/div[1]")
                userInfo = parse_user_info(userInfo.text)
                if (userInfo[1] in otherUsers): #jika sudah pernah di add, skip!
                    continue
                cursor = execute_sql(insertAccSQL, userInfo)
                print("Inserted new account : %s (%s)" % (userInfo[1], cursor.lastrowid))
                otherUsers.append(userInfo[1])
                continue

            lastTweetTime = waktu.timestamp()
            tweetLink: str = waktuEl.find_element_by_xpath("..").get_attribute("href")
            tweetID = tweetLink[(tweetLink.rfind("/") + 1):]
            print(waktu.ctime(), '=>', tweetID)
            execute_sql(insertSQL, (int(tweetID), selectedUser[0]))
            #print(artikel.find_element_by_css_selector("div.css-1dbjc4n.r-m611by").text)
        driver.execute_script("window.scrollBy(0, 700);")
        time.sleep(3)
    execute_sql("UPDATE tw_accounts SET status=2 WHERE id = %s", (selectedUser[0], ))
    #db.commit()

print("Done!")
driver.close()
driver.quit()
