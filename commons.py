import mysql.connector
import config

db = mysql.connector.connect(host=config.DB_HOST, user=config.DB_USER, password=config.DB_PASS, database=config.DB_NAME)
db.autocommit = True

def execute_sql(query: str, params=None):
    '''Melakukan eksekusi query SQL, sekalian manajemen koneksi
    Return cursor'''
    global db
    if not db.is_connected():
        db.reconnect()
    cursor = db.cursor()
    cursor.execute(query, params)
    return cursor

def parse_user_info(info: str):
    '''Nge-parse info user dimana terdiri dari dua baris
    - Baris 1 : Nama pengguna
    - Baris 2 : Username pengguna
    Jika terdiri dari satu baris, maka diasumsikan nama pengguna kosong
    Jika terdiri lebih dari dua baris, diasumsikan baris terakhir adalah username dan baris lainnya adalah nama pengguna
    
    Akan mengembalikan array [nama, username]'''
    userInfo = info.split("\n")
    if (len(userInfo) == 1):
        userInfo.append(userInfo[0])
        userInfo[0] = ''
    elif (len(userInfo) > 2):
        userName = userInfo.pop()
        screenName = " ".join(userInfo)
        userInfo = [screenName, userName]
    userInfo[0] = userInfo[0].encode("ascii", "ignore").decode() #biar unicode (huruf aneh aneh) gak masuk DB, gak begitu penting
    userInfo[1] = userInfo[1][1:]
    return userInfo